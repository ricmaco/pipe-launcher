import os, logging

from flask import Flask

from db.config import ConfigDAO
from db.data import DataDAO

if "gunicorn" in os.environ.get("SERVER_SOFTWARE", ""):
    DEBUG = False
    LOG = logging.INFO
else:
    DEBUG = True
    LOG = logging.DEBUG

App = Flask(__name__)
App.url_map.strict_slashes = False

# configure logging
config = ConfigDAO(debug=DEBUG).get()
assert 'log' in config
logging.basicConfig(
    filename=config['log'],
    format='%(asctime)s|%(levelname)s|%(name)s: %(message)s',
    level=LOG
)

# configure data database
DataDAO(debug=DEBUG)

# configure namspaces
from api import IndexBlueprint, MainApiBlueprint

App.register_blueprint(IndexBlueprint)
App.register_blueprint(MainApiBlueprint)

if __name__ == '__main__':
    App.run(host='0.0.0.0', debug=True)
