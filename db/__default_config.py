default = {
    # where to put log file
    'log': '/tmp/pipe-launcher.log',
    # whether to show settings page
    'show-settings': True
}