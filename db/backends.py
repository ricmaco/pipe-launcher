import sys
from os.path import isfile, expandvars
from threading import Lock
from contextlib import contextmanager

import toml
from singleton_decorator import singleton

PATHS = (
    'config.toml',
    expandvars('$HOME/.config/pipe-launcher/config.toml'),
    '/etc/pipe-launcher/config.toml',
)

@singleton
class TomlDB():

    def __init__(self, debug=False):
        '''Read TOML file from one of the paths in PATHS'''
        self.toml = None
        self.toml_path = None
        self.debug = debug
        self.lock = Lock()
        
        if not self.debug:
            while self.toml is None:
                for path in PATHS:
                    if isfile(path):
                        self.toml_path = path
                        try:
                            self.__read_toml(path)
                            print('Config loaded from %s' % path, file=sys.stderr)
                            return # toml loaded correctly, can exit
                        except:
                            print('Cannot load config from %s' % path, file=sys.stderr)
                
                # here no toml file can be found, so create it and use it
                self.toml_path = PATHS[0]
                self.__read_toml(PATHS[0], create=True)
                print('Created config file in %s' % PATHS[0], file=sys.stderr)
        else:
            self.toml = {}
            print('Using in-memory config', file=sys.stderr)
    
    def __read_toml(self, path, create=False):
        if create:
            # create an empty file
            open(path, 'a').close()
        self.toml = toml.load(path)

    def __write_toml(self):
        if not self.debug:
            with open(self.toml_path, 'w') as file:
                toml.dump(self.toml, file)

    def read(self, root=None):
        '''Read from TOML file, optionally returning only value for key root'''
        ret = None
        with self.lock:
            # base case, return whole toml
            if root is None:
                ret = self.toml
            try:
                # best case, root exists, so return value
                ret = self.toml[root]
            except:
                # worst case, return nothing
                pass
        return ret

    def write(self, object, root=None):
        '''Ẃrite an object to TOML file, under optional key root'''
        with self.lock:
            if root is None:
                # write into whole toml
                self.toml = object
            else:
                # write under root key
                self.toml[root] = object
            self.__write_toml()
    
    @contextmanager
    def transaction(self):
        '''Perform callback operation on TOML file, transactionally'''
        with self.lock:
            try:
                yield self.toml
            finally:
                self.__write_toml()

