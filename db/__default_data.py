default = {
    'title': 'Pipe | Launcher',
    'general': {
        'delay': 2000
    },
    'rows': [
        {
            'label': 'Label 1',
            'type': 'AND',
            "buttons": [
                {
                    'label': 'Button 1',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 2',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 3',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 4',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 5',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 6',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 7',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 8',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                }
            ]
        },
        {
            'label': 'Label 2',
            'type': 'TOGGLE',
            "buttons": [
                {
                    'label': 'Button 1',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 2',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 3',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 4',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 5',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 6',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 7',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 8',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                }
            ]
        },
        {
            'label': 'Label 3',
            'type': 'OR',
            "buttons": [
                {
                    'label': 'Button 1',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 2',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 3',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 4',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 5',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 6',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 7',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                },
                {
                    'label': 'Button 8',
                    'description': 'Very long description to display on hover',
                    'command-on': 'ls /',
                    'command-off': 'ls /pippo'
                }
            ]
        }
    ]
}