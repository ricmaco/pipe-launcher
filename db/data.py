import sys

from singleton_decorator import singleton

from .backends import TomlDB
from .__default_data import default

@singleton
class DataDAO():

    def __init__(self, debug=False):
        self.root = 'data'
        self.debug = debug

        self.db = TomlDB(debug=self.debug)

        # init with default data
        if self.db.read(root=self.root) is None:
            self.db.write(default, root=self.root)
            print('Initialized db with default data', file=sys.stderr)

    def get(self):
        '''Fetch data object from db'''
        return self.db.read(root=self.root)

    def set(self, data):
        '''Update data object with new in db'''
        self.db.write(data, root=self.root)

    def set_active(self, row, col, active):
        '''Set active/inactive button in db'''
        with self.db.transaction() as data:
            assert row < len(data[self.root]['rows'])
            assert col < len(data[self.root]['rows'][row]['buttons'])

            data[self.root]['rows'][row]['buttons'][col]['active'] = active
