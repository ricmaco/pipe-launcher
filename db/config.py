import sys

from singleton_decorator import singleton

from .backends import TomlDB
from .__default_config import default


@singleton
class ConfigDAO():

    def __init__(self, debug=False):
        self.root = 'config'
        self.debug = debug

        self.db = TomlDB(debug=self.debug)

        # init with default data
        if self.db.read(root=self.root) is None:
            self.db.write(default, root=self.root)
            print('Initialized db with default config', file=sys.stderr)

    def get(self):
        '''Fetch config object from db'''
        return self.db.read(root=self.root)

    def set(self, data):
        '''Update config object with new in db'''
        self.db.write(data, root=self.root)