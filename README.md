# Pipe | Launcher

Pipe | Launcher is a configurable command launcher dashboard. It allows spawning commands in a remote server, via ReST API.

## Installation

Follow this steps in order to install the application.

1. Download sources archive:
```bash
$ cd /opt
$ wget https://gitlab.com/ricmaco/pipe-launcher/-/archive/master/pipe-launcher-master.tar.gz
```

2. Unpack sources archive:
```bash
$ tar -xvf pipe-launcher-master.tar.gz
$ mv pipe-launcher-master pipe-launcher
$ rm -rf pipe-launcher-master.tar.gz
```

3. Install dependencies:
```bash
$ cd pipe-launcher
$ pip3 install -r requirements.txt
```

4. Try it (works from memory and does not create any configuration):
```bash
$ python app.py
Using in-memory config
Initialized db with default config
Initialized db with default data
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
Using in-memory config
Initialized db with default config
Initialized db with default data
```
`CTRL+C` to stop it.

## Start application

The right way to start application is using `gunicorn`, a multithread WSGI server.

If you are in the same folder as the application:
```bash
$ gunicorn wsgi:App
```

If you wish to make it world available and change default port, you should bind it to `0.0.0.0` and desired port:
```bash
$ gunicorn --bind 0.0.0.0:6666 wsgi:App
```

If you start it from another folder:
```bash
$ gunicorn --chdir /opt/pipe-launcher wsgi:App
```

Then `CTRL+C` to stop it.

At this point application has created a config file `config.toml` in the `/opt/pipe-launcher` folder.
Application will search for this file in one of the follwing locations (the first one found is used):

- `/opt/pipe-launcher/config.toml` (or whatever is the application folder)
- `$HOME/.config/pipe-launcher/config.toml`
- `/etc/pipe-launcher/config.toml`

You can move the created `config.toml` file in one of these locations and then restart application with one of the `gunicorn` commands.