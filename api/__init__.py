from flask import Blueprint

import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import flask.scaffold
flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func
from flask_restplus import Api

from .index import IndexBlueprint
from .data import DataApi
from .command import CommandApi

MainApiBlueprint = Blueprint('api', __name__, url_prefix='/api')
MainApi = Api(
    MainApiBlueprint,
    title='Pipe Launcher API',
    description='Specialized settings and command launcher API',
    version='1.0.0',
    doc="/docs"
)

MainApi.add_namespace(DataApi, path='/data')
MainApi.add_namespace(CommandApi, path='/command')