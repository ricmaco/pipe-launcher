import subprocess as sp

from flask import current_app as app

import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from flask_restplus import Namespace, Resource, fields

CommandApi = api = Namespace('command', description='Command launcher')

CommandRequest = api.model('CommandRequest', {
    'command': fields.String(required=True, description='Command to execute')
})
CommandResponse = api.model('CommandResponse', {
    'command': fields.String(required=True, description='Command to execute'),
    'output': fields.String(required=True, description='Command output'),
    'error': fields.Boolean(required=True, description='If error or not')
})

@api.route('/')
class Command(Resource):

    @api.doc('launch_command')
    @api.expect(CommandRequest)
    @api.marshal_with(CommandResponse)
    def post(self):
        '''Launch command from shell'''
        response = {
            'command': api.payload['command'],
            'output': None,
            'error': False
        }

        try:
            response['output'] = sp.check_output(
                api.payload['command'],
                stderr = sp.STDOUT,
                shell = True
            )
        except sp.CalledProcessError as e:
            response['error'] = True
            response['output'] = e.output

        app.logger.info('Launched command %s', response)

        return response
