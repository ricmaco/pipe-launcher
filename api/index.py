from version import VERSION_STRING

from flask import Blueprint, render_template

from db.config import ConfigDAO
from db.data import DataDAO

IndexBlueprint = bp = Blueprint('index', __name__, url_prefix='/')
config = ConfigDAO().get()
data = DataDAO().get()

@bp.route('/')
def register():
    '''Load index.html template'''
    return render_template('index.html',
        title=data['title'],
        version=VERSION_STRING,
        show_settings=config['show-settings']
    )
