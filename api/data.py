import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from flask_restplus import Namespace, Resource

from db.data import DataDAO

DataApi = api = Namespace('data', description='CRUD for data storage')
data = DataDAO()

@api.route('/')
class Data(Resource):

    @api.doc('fetch_data')
    def get(self):
        '''Fetch data from DB'''
        return data.get()

    @api.doc('save_data')
    def post(self):
        '''Fetch data from DB'''
        data.set(api.payload)
        return None, 204

@api.route('/active')
class DataActive(Resource):

    @api.doc('save_active_element')
    def put(self):
        '''Fetch data from DB'''
        row, col = api.payload['row'], api.payload['col']
        active = api.payload['active']

        data.set_active(row, col, active)
        return None, 204

